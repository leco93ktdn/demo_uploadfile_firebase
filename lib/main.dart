import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as Path;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.cyan,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<File> _image = [];
  List<String> _uploadedFileURL = [];
  bool isLoad = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Firestore File Upload'),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Selected Image'),
                _image.length > 0
                    ? SizedBox(
                        child: Expanded(
                        child: GridView.count(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          crossAxisCount: 3,
                          children: List.generate(_image.length, (index) {
                            return Center(
                                child: Container(
                                    height: 100,
                                    width: 100,
                                    child: Image.file(
                                        File(_image[index]?.path ?? ''))));
                          }),
                        ),
                      ))
                    : Container(),
                Row(
                  children: [
                    RaisedButton(
                      child: Text('Choose File'),
                      // onPressed: _getImageList,
                      onPressed: chooseFile,
                      color: Colors.cyan,
                    ),
                    _image.length > 0
                        ? RaisedButton(
                            child: Text('Upload File'),
                            onPressed: uploadFile,
                            color: Colors.cyan,
                          )
                        : Container(),
                    _image.length > 0
                        ? RaisedButton(
                            child: Text('Clear Selection'),
                            onPressed: clearSelection,
                          )
                        : Container(),
                  ],
                ),
                Text('Uploaded Image'),
                _uploadedFileURL != null
                    ? SizedBox(
                        child: Expanded(
                        child: GridView.count(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          crossAxisCount: 3,
                          children:
                              List.generate(_uploadedFileURL.length, (index) {
                            return Center(
                                child: Container(
                                    height: 100,
                                    width: 100,
                                    child: Image.network(
                                      _uploadedFileURL[index],
                                      height: 100,
                                    )));
                          }),
                        ),
                      ))
                    : Container(),
              ],
            ),
          ),
          Visibility(
            child: Center(
              child: CircularProgressIndicator(),
            ),
            visible: isLoad,
          )
        ],
      ),
    );
  }

  Future chooseFile() async {
    FilePickerResult result = await FilePicker.platform.pickFiles(
      allowMultiple: true,
      type: FileType.custom,
      allowedExtensions: ['jpg', 'mp4', 'mp4a'],
    );
    if (result != null) {
      List<File> files = result.paths
          .map((path) => File(path))
          .toList(); //allow multiple select file
      // File file = File(result.files.single.path); //just allow select one file
      setState(() {
        if (files != null) {
          _image = files;
        }
      });
    } else {
      // User canceled the picker
    }
  }

  Future uploadFile() async {
    setState(() {
      isLoad = true;
    });
    for(var file in _image) {
      await upload(file);
    }
    setState(() {
      isLoad = false;
    });
  }

  upload(File file) async {
    StorageReference storageReference = FirebaseStorage.instance
        .ref()
        .child('images/${Path.basename(file?.path ?? '')}}');
    StorageUploadTask uploadTask = storageReference.putFile(file);
    await uploadTask.onComplete;
    print('File Uploaded');
    storageReference.getDownloadURL().then((fileURL) {
      setState(() {
        _uploadedFileURL.add(fileURL);
      });
    });
  }

//   Future uploadFile() async {
// //for loop code
//     for (var file in _image) {
//       String path = file.path;
//       var filename = path.split("/").last;
//       upload(filename, path);
//     }
//   }
//
// //Single or Multiple flie upload firebase
//   upload(fileName, filePath) async {
//     _tasks = <StorageUploadTask>[];
//     StorageReference storageRef =
//         FirebaseStorage.instance.ref().child(fileName);
//     final StorageUploadTask uploadTask = storageRef.putFile(File(filePath));
//     _tasks.add(uploadTask);
//     await uploadTask.onComplete;
//     if (uploadTask.isComplete) {
//       _tasks.forEach((StorageUploadTask task) async {
//         final String url = await task.lastSnapshot.ref.getDownloadURL();
//         print('Tag' + url);
//       });
//     }
//   }

  void clearSelection() {
    setState(() {
      _image = [];
      _uploadedFileURL = [];
    });
  }
}
